<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

/**
 * @return array<int, array>
 *
 * @throws CountryLoaderException
 */
function get_countries_and_dictionary(): array
{
    $countries = \Rinvex\Country\CountryLoader::countries(true, true);

    $collect_countries = [];
    $dictionary = [];
    foreach ($countries as $country) {
        /**
         * @var \Rinvex\Country\Country $country
         */
        $code = mb_strtolower($country->getIsoAlpha2());

        // English version
        $name = $country->getName();
        $name_official = $country->getOfficialName();
        if ($name != $name_official) {
            $dictionary[] = ["search" => $name_official, "code" => $code];
        }

        // Russian version
        $name_ru = $country->getTranslation('rus')["common"];
        $name_official_ru = $country->getTranslation('rus')["official"];
        if ($name_ru != $name_official_ru) {
            $dictionary[] = ["search" => $name_official_ru, "code" => $code];
        }

        $collect_countries[] = [
            "code" => $code,
            "title_en" => $name,
            "title_ru" => $name_ru
        ];
    }

    return [$collect_countries, $dictionary];
}

function main(): void
{
    try {
        list($countries, $dictionary) = get_countries_and_dictionary();

        var_dump($dictionary);
        var_dump($countries);
    } catch (\Rinvex\Country\CountryLoaderException $e) {
        printf("Error countries: %s", $e->getMessage());
    }
}

main();
